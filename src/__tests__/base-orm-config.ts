import { TypeOrmModuleOptions } from '@nestjs/typeorm';
import { TestRelatedEntity } from './test-related.entity';
import { TestEntity } from './test.entity';

export const baseOrmConfigs: TypeOrmModuleOptions = {
  entities: [TestEntity, TestRelatedEntity],
  host: 'localhost',
  port: 5432,
  type: 'postgres',
  username: 'postgres',
  password: '1234512345',
  database: 'test',
};
